

import java.io.IOException;

public class TttoolExecutor {

    private static final String pathToTTTool = "src/test/testresources/tttool-win32-1.8/tttool.exe";

    public static String generateGMEFile(String file) throws IOException, InterruptedException {
    	System.out.println(file);
        String[] cmd = {pathToTTTool, "assemble", file};
        Process p = Runtime.getRuntime().exec(cmd);
        p.waitFor();
        if (p.exitValue() != 0) {
            System.out.println(convertStreamToString(p.getErrorStream()));
            throw new RuntimeException(convertStreamToString(p.getInputStream()));
        }
        System.out.println("done");
        return file.replace(".yaml", ".gme");
    }
    
    public static String generateOIDCodes(String file) throws IOException, InterruptedException {
    	System.out.println(file);
        String[] cmd = {pathToTTTool, "oid-codes", file};
        Process p = Runtime.getRuntime().exec(cmd);
        p.waitFor();
        if (p.exitValue() != 0) {
            System.out.println(convertStreamToString(p.getErrorStream()));
            throw new RuntimeException(convertStreamToString(p.getInputStream()));
        }
        System.out.println("done");
        return file.replace(".yaml", ".yaml");
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public static String getHelp() throws IOException, InterruptedException {
        String[] cmd = {pathToTTTool, "--help"};
        Process p = Runtime.getRuntime().exec(cmd);
        p.waitFor();
        if (p.exitValue() != 0) {
            System.out.println(convertStreamToString(p.getErrorStream()));
            throw new RuntimeException(convertStreamToString(p.getInputStream()));
        }
        return convertStreamToString(p.getInputStream());
    }

    public static void main(String[] args) {
        String pathToFile = "C:\\Users\\Damian Hettmanczyk\\OneDrive\\tttt\\TTProgramm\\src\\main\\resources\\lndwguide.yaml";
        try {
        	System.out.println(pathToFile);
        		//generateOIDCodes(pathToFile);
            generateGMEFile(pathToFile);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
