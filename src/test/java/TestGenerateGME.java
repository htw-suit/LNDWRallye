

import org.testng.annotations.Test;


import java.io.IOException;

public class TestGenerateGME {

    @Test
    public void testGenerateGMELNDWGuideYAML() {
        String file = "D:\\Studium\\SUIT\\LNDW\\src\\main\\resources\\lndwguide.yaml"; // Path to .yaml
        //String file = "example.yaml";
        try {
           TttoolExecutor.generateGMEFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetHelp() throws IOException, InterruptedException {
        System.out.println(TttoolExecutor.getHelp());
    }

}
